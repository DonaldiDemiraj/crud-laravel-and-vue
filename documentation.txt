1.clone project from git repositiory

2.run composer install

3.run php artisan

4.npm install

5.php artisan migrate


Komandat per krijimin e projektit nga fillimi

1- laravel new employees
2- cd employees
3- composer require laravel/ui
4- php artisan ui bootstrap --auth
5- npm install && npm run dev
6- npm run dev
7- php artisan serve
8- php artisan ui vue
9- npm install && npm run dev
10- npm run dev
11- php artisan make:model Emplayee -m
12- php artisan make:model Country -m
13- php artisan make:model State -m
14- php artisan make:model City -m
15- php artisan make:model Departament -m
16- php artisan migrate

17- create table in migration for database
18- php artisan migrate:fresh

19- https://starbootstrap.com for dashboard(index.html)
20-(create new view) - resource/views/layouts/main.blade.php
21- tek main.blade.php implement fontawesome cdnjs link
22- tek home.blade.php ndryshojme @extends('layouts.app) ne @extends('layouts.main)
23- tek public/css krijojme filen e stilit sb-admin.min.css te cilin e marim nga fili i bootstrap
24- te njejten gje bejme dhe per js
25- per ta thirue ne main.blade.php <link href="{{asset(css/sb-admin.min.css)}}" rel="stylesheet" />
26- ndersa per app.js <script src="{{mix('js/app.js')}}"></script>
27- posht dashboard theresim <div class="row">@yeild('content')</div>
28- tek top navbar tek span {{Auth::user()->username}}
dhe posht saj tek tagu a href nga app.blade.php marim copy pjesen e logout dhe e bejme paste

29- Tek RegisterController,register.blade.php,si dhe tek user.php model  pershtasim fushat si tek tabela e migrimeve
30- tek LoginController.php shtojme: protect $maxAttempts = 3 dhe protect $decayMinutes = 5; per pjesen nqs useri logohet 3 here gamin do duhet te prese 5 min

31- Tani krijojme kontrollerat me radhe
php artisan make:controller Backend/UserController --resource(keshtu per te gjith kontrollerat)
32- tek web.php (route file) Route::resource('user',UserController::class)
33- tek main.blade.php pjesa e link e vendosim href={{route(usere.index)}} qe do te thote
emri i filderit qe ndertojme tek views dhe emri i view qe ndertojme brenda ketij folderi index.blade.php

34- krijojme request
php artisan make:request UserStoreRequest
tek i cili ndryshojme authorizate qe kthen return true;
dhe tek rules vendosim required per fushat e tabeles user qe kemi krijuar ne database

krijojme dhe nje request per update  php artisan make:request UserUpdateRequest


per pjesen e vue installojme vue-router
npm install vue-router
